#include <SDL.h>
#include <chrono>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
typedef std::pair<std::string, float> eventStamp;
using namespace std::chrono;

template <typename T>
auto timediff_stamp(const T start){
	return [=](){
		return duration_cast<milliseconds>(system_clock::now() - start).count();
	};
}

std::string parseEvent(const SDL_Event& e){
	std::stringstream ss;
	switch(e.type){
	case SDL_JOYAXISMOTION:
		ss << "J" << int(e.jaxis.which) << "_A" << int(e.jaxis.axis) << " (" << int(e.jaxis.value) << ")";
		break;
	case SDL_JOYHATMOTION:
		ss << "J" << int(e.jhat.which) << "_H" << int(e.jhat.hat) << " (" << int(e.jhat.value) << ")";
		break;
	case SDL_JOYBUTTONDOWN:
		ss << "J" << int(e.jbutton.which) << "_B" << int(e.jbutton.button) << " (+)";
		break;
	case SDL_JOYBUTTONUP:
		ss << "J" << int(e.jbutton.which) << "_B" << int(e.jbutton.button) << " (-)";
		break;
	case SDL_QUIT:
		ss << "Quit Signal Received";
		break;
	case SDL_JOYDEVICEADDED:
		if(int w = e.jdevice.which; SDL_JoystickOpen(w))
			ss << "New joystick: " << w;
		break;
	}
	return ss.str();
}

int main(int argc, char** argv){
	bool quit = false;
	auto t = system_clock::now();

	if(SDL_Init(SDL_INIT_JOYSTICK) < 0){
		std::cerr << "Failed to initialize SDL_Joystick. Aborting\n";
		return -1;
	}
	for(int i = 0; i < SDL_NumJoysticks(); ++i)
		SDL_JoystickOpen(i);
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");
	std::vector<eventStamp> log_events;
	auto stopwatch = timediff_stamp(t);
	log_events.emplace_back("Logging started", stopwatch());
	std::stringstream ss;
	ss << "inp_" << std::setw(4) << std::setprecision(4)
	   << duration_cast<nanoseconds>(t.time_since_epoch()).count();
	do {
		SDL_Event evhandle;
		std::vector<SDL_Event> evs;
		while(SDL_PollEvent(&evhandle))
			evs.emplace_back(evhandle);
		for(auto& ev:evs)
			if(auto elog = parseEvent(ev); !elog.empty())
				log_events.emplace_back(elog, stopwatch());
		
	} while(log_events.back().first != "Quit Signal Received");

	std::ofstream read;
	read.open(ss.str() + ".log");
	for(auto& [ev, time] : log_events)
		read << std::setfill('0') << std::setw(12) << time << "ms: " << ev << std::endl;
	read.close();
	return 0;
}
